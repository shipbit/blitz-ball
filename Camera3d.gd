extends Camera3D

@export var scroll_speed := 10
@export var camera_min_zoom := 10.0
@export var camera_max_zoom := 28.0
@export var zoom_step := 0.5
@export var rotation_speed := -0.002

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("camera_up"):
		translate(Vector3(0, 0, -scroll_speed * delta))
	if Input.is_action_pressed("camera_down"):
		translate(Vector3(0, 0, scroll_speed * delta))
	if Input.is_action_pressed("camera_left"):
		translate(Vector3(-scroll_speed * delta, 0, 0))
	if Input.is_action_pressed("camera_right"):
		translate(Vector3(scroll_speed * delta, 0, 0))

	if Input.is_action_pressed("camera_rotate_left"):
		rotate(Vector3(0, 1, 0), 1 * delta)
	if Input.is_action_pressed("camera_rotate_right"):
		rotate(Vector3(0, 1, 0), -1 * delta)


func _input(event):
	if event is InputEventMouseMotion:
		if Input.is_action_pressed("camera_rotate"):
			rotate(Vector3(0, 1, 0), event.relative.x * rotation_speed)
	if event is InputEventMouseButton:
		if Input.is_action_pressed("camera_zoom_in") or Input.is_action_pressed("camera_zoom_out"):
			var new_zoom = zoom_step if Input.is_action_pressed("camera_zoom_in") else -zoom_step
			size = clamp(size + new_zoom, camera_min_zoom, camera_max_zoom)
	
	
