extends Node

class_name Blitzball


func _on_stadium_goal(side: Stadium.Team):
	score_goal(side)
	reset_turn()

func _on_stadium_ball_out():
	print("out")
	reset_turn()
	
func score_goal(side: Stadium.Team):
	print("goal %s"%("Home" if side == Stadium.Team.HOME else "Guest"))

func reset_turn():
	$Ball.reset()
	var players = $Players.get_children()
	for player in players:
		player.position = create_position()

func create_position():
	const stadium_size = Vector2i(60,36)
	const margin = Vector2i(4,4)
	const placement_range = stadium_size - margin

	var x = randi() % placement_range.x - (placement_range.x / 2)
	var z = randi() % placement_range.y - (placement_range.y / 2)
	
	return Vector3(x,5,z)
	

