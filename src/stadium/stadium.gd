extends Node3D
class_name Stadium


enum Team {HOME, GUEST}

signal goal(side)
signal ball_out

func _on_goal_area_body_entered(body:Node3D, is_home:bool):
	if body.is_in_group("ball"):
		goal.emit(Team.HOME if is_home else Team.GUEST)


func _on_area_3d_body_exited(body:Node3D):
	if body.is_in_group("ball"):
		ball_out.emit()
