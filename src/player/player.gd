extends RigidBody3D

class_name Player

@export var color = Color.RED
@export var influence_transparency = 0.2
@onready var influenceMesh = %InfluneceMesh
@onready var bodymesh = %Mesh

func _ready():
	influenceMesh.mesh.material.set_shader_parameter('albedo',Color(color.r,color.g,color.b,influence_transparency))
	bodymesh.mesh.material.set_shader_parameter('albedo', color)

func _on_influence_area_body_exited(body:Node3D):
	if body.is_in_group("ball"):
		influenceMesh.visible = false

func _on_influence_area_body_entered(body:Node3D):
	if body.is_in_group("ball"):
		influenceMesh.visible = true
