@tool
extends Node

class_name Throwable

@export var ray_length: float = 100
@export var pickup_constant_torque: Vector3 = Vector3.ZERO
@export var mouse_velocity_scale: float = 0.02

var is_dragging = false
var hovered = false
var draggable_node: CollisionObject3D

signal picked(node:CollisionObject3D)
signal thrown(node: CollisionObject3D)
signal drag_move(node:CollisionObject3D, cast: Dictionary)

func _get_configuration_warnings():
	if get_parent() is CollisionObject3D:
		return []

	return ['Not under a collision object']

# Called when the node enters the scene tree for the first time.
func _ready():
	set_physics_process(false)

	if Engine.is_editor_hint():
		set_process(false)
		return

	draggable_node = get_parent()
	draggable_node.input_capture_on_drag = true
	draggable_node.mouse_entered.connect(self.draggable_node_mouse_entered)
	draggable_node.mouse_exited.connect(self.draggable_node_mouse_exited)
	draggable_node.input_event.connect(self.draggable_node_input_event)


func _physics_process(_delta):
	if not is_dragging:
		return;

	var viewport = get_viewport()
	var camera =viewport.get_camera_3d()
	var mouse_position = viewport.get_mouse_position()
	var from = camera.project_ray_origin(mouse_position)
	var to = from + camera.project_ray_normal(mouse_position) * ray_length
	var physics_query_paramaters = PhysicsRayQueryParameters3D.create(
		from,
		to,
		draggable_node.get_collision_mask(),
		[draggable_node]
	)
	
	var cast = viewport.world_3d.direct_space_state.intersect_ray(physics_query_paramaters)
	if not cast.is_empty():
		on_move(cast)

func draggable_node_mouse_entered():
	hovered = true

func draggable_node_mouse_exited():
	hovered = false

func draggable_node_input_event(_camera, event, _position, _normal, _shape_idx):
	if event.is_action('select'):
		if event.is_pressed():
			if hovered:
				on_picked()
		elif is_dragging:
			if is_dragging:
				on_released()

func on_picked():
	is_dragging = true
	set_physics_process(true)
	draggable_node.linear_velocity = Vector3.ZERO
	draggable_node.angular_velocity = Vector3.ZERO
	if draggable_node is RigidBody3D:
		draggable_node.constant_torque = pickup_constant_torque 
	picked.emit(draggable_node)

func on_released():
	is_dragging = false
	set_physics_process(false)
	draggable_node.linear_velocity = Vector3.ZERO
	draggable_node.angular_velocity = Vector3.ZERO
	if draggable_node is RigidBody3D:
		draggable_node.constant_torque = Vector3.ZERO
	var mouse_velocity = Input.get_last_mouse_velocity() * mouse_velocity_scale
	var force  = Vector3(mouse_velocity.x,0,mouse_velocity.y)
	draggable_node.apply_central_impulse(force)
	thrown.emit(draggable_node)

func on_move(cast: Dictionary):
	cast.position.y = draggable_node.position.y
	draggable_node.move_and_collide(cast.position - draggable_node.position)
	drag_move.emit(draggable_node, cast)
